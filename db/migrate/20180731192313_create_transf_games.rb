class CreateTransfGames < ActiveRecord::Migration[5.2]
  def change
    create_table :transf_games do |t|
      t.boolean :status
      t.integer :friend_id
      t.integer :user_id
      t.references :my_game, foreign_key: true

      t.timestamps
    end
    #se eu tentar dar db:migate dara erro porque nao ta aqui referenciado
    # obs nao pode se evitar cirar classe como no JAVA
    add_index "transf_games", ["user_id"], name: "index_transf_games_on_user_id", using: :btree
    add_index "transf_games", ["friend_id"], name: "index_transf_games_on_friend_id", using: :btree
  end
end
