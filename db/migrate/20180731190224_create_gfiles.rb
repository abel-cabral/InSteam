class CreateGfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :gfiles do |t|
      t.string :attachement
      t.string :name
      t.references :games, foreign_key: true

      t.timestamps
    end
  end
end
