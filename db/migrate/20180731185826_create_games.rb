class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      t.string :description
      t.float :price
      t.string :picture

      t.timestamps
    end
  end
end
