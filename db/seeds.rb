# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def loop_user 
        User.create(
        name: Faker::Name.name, 
        email: Faker::Internet.email,
        password: '12345',
        password_confirmation: '12345',
        #Como user depende de regiao, chamamos ela na criacao do usuario
        region_id: rand(1..12))
end

def loop_game 
        Game.create(
        name: Faker::Color.color_name, 
        description: Faker::Lovecraft.fhtagn(3),
        price: rand(25.0..238.0),
        picture: Faker::Movie.quote)
end

def loop_region
        Region.create(name: Faker::Nation.nationality)
end

#Faz o looping da funcao que queremos
50.times do |t|
        #chamamos a funcao com () para executar    
        loop_region()    
        loop_user()
        #loop_game()           
end