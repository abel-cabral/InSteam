Rails.application.routes.draw do  
  get 'sessions/new'
  resources :categories
  resources :games
  resources :users
  resources :posts

  #Se logado como ADM  

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  post '/comprar/:id', to: 'games#comprar_game', as: :comprar_game
  #lista jogos comprados
  get 'livraria', to: 'users#my_games'
  #Troca de games    
#  get 'transferencia', to 'users_helper#trans_my_game'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
