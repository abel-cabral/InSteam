module SessionsHelper
#Criamos aqui a funcao que ẽ usada para startar uma sessao
    def log_in(user)
        #setamos o id do usuario como ancora da sessao
        session[:user_id] = user.id
    end    


    #Returns the current logged-in user (if any).
    def current_user
        #Pega o id do usuario logado, acima atribuido a variavel e pesquisa no BD, virando um
        #objeto com todos os dados dele
        @current_user ||= User.find_by(id: session[:user_id])
    end


    # Retorna True para usuario Logado e False pra deslogado
    def logged_in?
        !current_user.nil?
    end
  

    # Encerrar Sessao
    def log_out
        #deleta o ID que estava na seesao        
        session.delete(:user_id)
        #limpa o objeto que tinha os dados
        @current_user = nil
    end


    def nao_logado
        if !logged_in?
            redirect_to login_path  
        end  
    end

    def ja_logado
        if logged_in?
            redirect_to user_path  
        end  
    end






end
