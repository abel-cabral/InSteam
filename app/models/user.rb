class User < ApplicationRecord
  belongs_to :region
  #Necessario para autenticacao
  has_secure_password
  validates :password, presence: true, length: { minimum: 5 }

  # Returns the hash digest of the given string.
  #Aqui acredito que fazemos a criptografia da senha e tbm usamos para comparar com a senha digitada
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
