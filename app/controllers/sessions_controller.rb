class SessionsController < ApplicationController  
  def new    
  end
  
  def create    
    #procura em nos usuarios quem tem o mesmo email digitado nessa sessao
    user = User.find_by(email: params[:session][:email].downcase)#downcase pra minusculo
    #Se o user encontrado tiver a senha igual a digitada, sucesso
    if user && user.authenticate(params[:session][:password])
      #Funcao que seta o login
      log_in user
      #E redireciona pra pagina correta
      redirect_to games_path
    else
      flash.now[:danger] = 'Combinação de <nome_email_etc> e senha inválida.'
      render 'new'
    end
  end
  
  def destroy    
    #Destroy a sessao
    log_out
    #Encaminha pra pagina X
    redirect_to login_path
  end
end
