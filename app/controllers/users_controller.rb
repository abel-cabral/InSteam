class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]    
  #Paginas onde nao é permitido acesso
  before_action :nao_logado, except: [:new]


   
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  def my_games
    #Em usar_id eu passo o 
    @my_games = MyGame.where(user_id: current_user.id)        
  end

  #Captura os dados preenchidos para submeter
  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    #User recebe ele mesmo todo minimizado, para evitar erros por letras maiusculas    
    @user.email = @user.email.downcase
    @user.name = @user.name.downcase       
    respond_to do |format|
      if @user.save
        log_in @user        
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :region_id, :password, :password_confirmation)
    end
end
